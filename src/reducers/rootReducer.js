import { combineReducers }  from 'redux'


import experiment           from './experiment'


const rootReducer = combineReducers({
  experiment: experiment.reducer,
})

export default rootReducer