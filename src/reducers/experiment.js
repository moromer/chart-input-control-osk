const name = `experiment`

const types = {
  UPDATE: `${ name }/UPDATE`
}

const data = {
  'bar1': { value: 10 },
  'bar2': { value: 25 },
  'bar3': { value: 55 },
  'bar4': { value: 75 },
  'bar5': { value: 95 },
}

const STATE = {
  data,
  order: Object.keys(data)
}

const reducer = (state = STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case types.UPDATE: {
      console.log('Inside Reducer UPDATE_STATE')
      const { data, order } = state
      const { id, value } = payload
      console.log(data)
      return {
        data: {
          ...data,
          [id]: {value: value},
        },
        order,
      }
    }
    default:
      return state
  }
}

export default {
  types,
  reducer,
}