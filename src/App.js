import React, { Component } from 'react'
import { Provider }         from 'react-redux'

import styled               from 'styled-components'


import PageHeader           from './components/PageHeader'
import PageHome             from './components/PageHome'
import store                from './store'


const AppMain = styled.div`
    text-align: center;
`

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <AppMain>
            <PageHeader />
            <PageHome />
        </AppMain>
      </Provider>
    )
  }
}

export default App