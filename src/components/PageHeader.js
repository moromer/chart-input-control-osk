import React, { Component } from 'react'
import styled,{ keyframes } from 'styled-components'

import Logo from '../logo.svg'


const frames = keyframes`
    from { transform: rotate(0deg); }
    to { transform: rotate(360deg); }
`
const LogoSpinner = styled.span`
    animation: ${frames} infinite 20s linear;
    height: 80px;
`    
const AppHeader = styled.div`
    justify-content: space-between;
    background-color: #222;
    padding: 5px;
    color: white;

    display: -webkit-flex;
    display: flex;
    -webkit-flex-direction: row; /* works with row or column */
    flex-direction: row;
    -webkit-align-items: center;
    align-items: center;
`
const UlMenu = styled.ul`
    list-style-type: none !important;
    display: flex;
`
const LiMenu = styled.li`
    margin: 0 10px;
`
const MenuRight = styled.div`
    margin: 10px;
    display: flex;
`
const MenuRightLogin = styled.div`
    margin: 0 50px;
`
const MenuRightLang = styled.div`
`
class PageHeader extends Component {
    render() {
        return (
            <AppHeader>
                <LogoSpinner><img src={Logo} width='80' height='80' alt='logo'/></LogoSpinner>
                <div>
                    <UlMenu>
                        <LiMenu>Card</LiMenu>
                        <LiMenu>Buy crypto</LiMenu>
                        <LiMenu>Add balance</LiMenu>
                        <LiMenu>Send money</LiMenu>
                        <LiMenu>Social activity</LiMenu>
                    </UlMenu>
                </div>
                <MenuRight>
                    <MenuRightLogin>Login</MenuRightLogin>
                    <MenuRightLang>En</MenuRightLang>
                </MenuRight>
            </AppHeader>
        );
    }
}

export default PageHeader