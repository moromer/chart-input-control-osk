import React, { Component } from 'react'
import { connect }          from 'react-redux'
import { compose }          from 'redux'


import Chart                from './Chart'
import Form                 from './Form'

import experiment           from '../reducers/experiment'


class MainContainer extends Component {
  onChange = ({ id, value }) => {
    const { dispatch } = this.props

    dispatch({
      type: experiment.types.UPDATE,
      payload: { id, value },
    })
  }
    
  render() {
    const { data, order } = this.props

    return (
      <div>
        <Chart data={ data } order={ order } />
        <Form data={ data } order={ order } onChange={ this.onChange } />    
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { experiment } = state

  return {
    data: experiment.data,
    order: experiment.order,
  }
}

export default compose(
  connect(mapStateToProps),
)(MainContainer)