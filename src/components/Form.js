import PropTypes            from 'prop-types'
import React, { Component } from 'react'


class Form extends Component {
  static PropTypes = {
    data: PropTypes.object.isRequired,
    order: PropTypes.array.isRequired,

    onChange: PropTypes.func,
  }
  
  constructor(props) {
    super(props)
    this.state = {
      id: '',
      value: '',
    }
  }

  onChange = ({ currentTarget: { value } }) => this.setState({ value })

  onSelect = ({ currentTarget: { value } }) => {
        this.setState({ id: value })
        console.log({value})
    }

  onSubmit = (event) => {
    event.preventDefault()

    const { onChange } = this.props
    const { id, value } = this.state
    console.log('onSubmit', 'onChange', onChange, 'this.props', this.props, 'this.State', this.state)
    onChange && onChange({ id, value })
  }

  render() {
    const { data, order } = this.props
    const { id, value } = this.state

    console.log(`Form`, `render`, `id`, id, `value`, value)
    
    return (
    <div>
       <select onChange={ this.onSelect }>
          {
            order.map((id) => {
              const { value } = data[id]
              return <option key={ id }>{ id }</option>
            })
          }
        </select>
            <label><br/>
          <input
            type='Number'
            value = { value }
            onChange={ this.onChange }
          />
        </label><br/>
        <input type='button' value='Submit' onClick={ this.onSubmit } />
    </div>
        )
  }
}
export default Form