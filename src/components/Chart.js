import React, { Component } from 'react'
import styled               from 'styled-components'


const Ul = styled.ul`
  display: inline-flex;
  list-style-type: none;
  align-items: flex-end;
`

const Li = styled.li`
  margin: 0 30px;
`

const Bar = styled.div`
  min-height: 100px; 
  display: inline;
  align-items: flex-end;
`

const BarLine = styled.div`
  background-color: aqua;
  height: ${ (props) => props.height }px;
  width: 20px;
  margin: 0 auto;
`

class Chart extends Component {
    
  renderItems() {
    const { data, order } = this.props
    
    return order.map((id, index) => {
      const { value } = data[id]
 
      return (
        <Li key={ index }>
          <Bar>
            <BarLine height={ value }></BarLine>
            <span>{ value }</span>
          </Bar>
          <div>
            { id }
          </div>
        </Li>
      )
    })
  }

  render() {

    return (
      <div>
          <Ul>
            { this.renderItems() }
          </Ul>
      </div>
    )
  }
}


export default Chart